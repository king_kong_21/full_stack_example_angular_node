import { Component, OnInit } from '@angular/core';
import { dashboardService } from '../../../../data/service/dashboardService.service';
import { SharedService } from '../../../../shared/services/SharedService.service';
import 'rxjs/Rx';
import { posts } from '../../../../data/schema/posts';
import { Router, ActivatedRoute } from '@angular/router'
import * as _ from 'lodash';
import { authService } from '../../../../data/service/authService.service';



@Component({
  selector: 'dashboard-module',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class dashboard implements OnInit {

  verType: string ="ver1";
  public viewTicketsServerResult: any[] = [];
  public viewTicketslLocalResult: any[] = [];
  public size:Number = null;
  public total_request:Number = null;
  public new_request:Number = null;
  public answered_request:Number = null;
  public solved_request:Number = null;
  public closed_request:Number = null;
  public unanswered_request:Number = null;


 constructor(private route:ActivatedRoute,private authService:authService){
    this.total_request = null;
    this.new_request = 25 ;
    this.answered_request = 100;
    this.solved_request = 25;
    this.closed_request = 75;
    this.unanswered_request = 35;
 }

  ngOnInit(){  
          this.getDataforDashboard({},"http://localhost:5000/api/viewtickets");
  }

  getDataforDashboard = (params,_Url) =>{
        this.authService.postData(params,_Url).subscribe(
          res =>{
             console.log("Ticket registration result")
             console.log(res)
             this.size= res["data"].length;
             this.total_request = res["data"].length;
             this.viewTicketsServerResult = res["data"]
             console.log(this.viewTicketsServerResult)
             console.log(this.size)
             
             let tmp ;
             if (this.viewTicketsServerResult) {
              this.viewTicketsServerResult.map((data_block,index)=>{
              
                tmp = {}
                 for (var key in data_block) {
                       if(key==="_id"){
                         tmp["Request_Id"] = data_block[key];
                       }else if(key==="ticket_chat_group_id"){
                         tmp["ticket_chat_group_id"] = data_block[key];
                       }else if(key==="ticket_catagory"){
                         tmp["Ticket_catagory"] = data_block[key];
                       }else if(key==="ticket_sub_catagory"){
                         tmp["Ticket_sub_catagory"] = data_block[key];
                       }else if(key==="ticket_action"){
                         tmp["Ticket_action"] = data_block[key];
                       }else if(key==="ticket_closingn_date"){
                         tmp["Ticket_closingn_date"] = data_block[key];
                       }else if(key==="ticket_creation_date"){
                         tmp["Ticket_creation_date"] = data_block[key];
                       }else if(key==="ticket_description"){
                         tmp["Ticket_description"] = data_block[key];
                       }else if(key==="ticket_incident_type"){
                         tmp["Ticket_incident_type"] = data_block[key];
                       }else if(key==="ticket_priority"){
                        tmp["Ticket_priority"] = data_block[key];
                      }else if(key==="ticket_status"){
                        tmp["Ticket_status"] = data_block[key];
                      }else if(key==="userid"){
                        tmp["Requester_id"] = data_block[key];
                      }
                 }  
                 this.viewTicketslLocalResult.push(tmp)
             })}
             console.log(this.viewTicketslLocalResult)
            },
          error => {
            console.log(error)
           }
          )
      }



      log_out_User(){
        this.authService.logout();
      }


    



}


